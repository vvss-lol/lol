const {pet, text} = require('../fixtures/params_search_valid.json');

describe('Lab5', () => {
    it('Valid search', () => {
        cy.visit('https://petstore.octoperf.com/actions/Catalog.action')

        cy.get('input[name="keyword"]').type(pet)
        cy.get('input[name="searchProducts"]').click()
        cy.contains(text)
    })
})