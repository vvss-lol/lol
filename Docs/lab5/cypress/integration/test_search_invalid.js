const {pet, text} = require('../fixtures/params_search_invalid.json');

describe('Lab5', () => {
    it('Invalid search', () => {
        cy.visit('https://petstore.octoperf.com/actions/Catalog.action')

        cy.get('input[name="keyword"]').type(pet)
        cy.get('input[name="searchProducts"]').click()
        cy.contains(text)
    })
})