const {username, password, text} = require('../fixtures/params_login_valid.json');
const {text1, text2, text3, text4, text5, text6} = require('../fixtures/params_proceed_to_checkout_flow.json');


describe('Lab5', () => {
    it('Flow1', () => {
        cy.visit('https://petstore.octoperf.com/actions/Catalog.action')
        cy.contains('Sign In').click();

        cy.get('input[name="username"]').type(username)
        cy.get('input[name="password"]').invoke('attr', 'value', '')
        .should('have.attr', 'value', '')
        cy.get('input[name="password"]').type(password)
        
        cy.get('input[name="signon"]').click()
        cy.contains(text)

        //Click on fish
        cy.get('a').eq(5).click()
        cy.contains(text1)

        //Click on first fish
        cy.get('a').eq(11).click()
        cy.contains(text2)

        //Click add to cart
        cy.get('a').eq(12).click()
        cy.contains(text3)

        //Click proceed to checkout
        cy.get('a').eq(13).click()
        cy.contains(text4)

        //Click on continue
        cy.get('input[name="newOrder"]').click()
        cy.contains(text5)

        //Click on confirm
        cy.get('a').eq(11).click()
        cy.contains(text6)

    })
})