const {username, password, text} = require('../fixtures/params_login_valid.json');

describe('Lab5', () => {
    it('Login valid', () => {
        cy.visit('https://petstore.octoperf.com/actions/Catalog.action')
        cy.contains('Sign In').click();

        cy.get('input[name="username"]').type(username)
        cy.get('input[name="password"]').invoke('attr', 'value', '')
        .should('have.attr', 'value', '')
        cy.get('input[name="password"]').type(password)
        
        cy.get('input[name="signon"]').click()
        cy.contains(text)
    })
})