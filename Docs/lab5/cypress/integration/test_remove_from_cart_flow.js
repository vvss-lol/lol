const {username, password, text} = require('../fixtures/params_login_valid.json');
const {text1, text2, text3, text4} = require('../fixtures/params_remove_from_cart_flow.json');

describe('Lab5', () => {
    it('Flow2', () => {
        cy.visit('https://petstore.octoperf.com/actions/Catalog.action')
        cy.contains('Sign In').click();

        cy.get('input[name="username"]').type(username)
        cy.get('input[name="password"]').invoke('attr', 'value', '')
        .should('have.attr', 'value', '')
        cy.get('input[name="password"]').type(password)
        
        cy.get('input[name="signon"]').click()
        cy.contains(text)

        //Click on dogs
        cy.get('a').eq(6).click()
        cy.contains(text1)

        //Click on first dog
        cy.get('a').eq(11).click()
        cy.contains(text2)

        //Click add to cart
        cy.get('a').eq(12).click()
        cy.contains(text3)

        //Click remove
        cy.get('a').eq(12).click()
        cy.contains(text4)

    })
})