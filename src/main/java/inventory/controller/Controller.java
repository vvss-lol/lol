package inventory.controller;

import inventory.service.InventoryService;
import javafx.scene.Parent;
import javafx.stage.Stage;

public abstract class Controller {


    protected Stage stage;
    protected Parent scene;

    public Controller(){}

    abstract void setService(InventoryService service);
}