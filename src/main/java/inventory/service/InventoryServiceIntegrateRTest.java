package inventory.service;

import inventory.exceptions.ValidationException;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.repository.Inventory;
import inventory.repository.PartRepository;
import inventory.repository.ProductRepository;
import inventory.validator.PartValidator;
import inventory.validator.ProductValidator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class InventoryServiceIntegrateRTest {

    public static PartRepository repo;
    public static InventoryService service;
    public static Inventory inventory;
    public static OutsourcedPart outsourcedPart;

    @BeforeEach
    void setUp() {
        inventory = new Inventory();
        repo = new PartRepository("data/parts_test.txt", inventory);
        service = new InventoryService(repo, null, new ProductValidator(),new PartValidator());
        outsourcedPart = mock(OutsourcedPart.class);
        Mockito.when(outsourcedPart.getName()).thenReturn("Test");
        repo.add(outsourcedPart);

    }

    @Test
    void lookupPart_WhenPartMock_returnNull() {
        Assertions.assertNull(service.lookupPart("Test1"));
    }

    @Test
    void lookupPart_WhenPartMock_PartFound() {
        Assertions.assertEquals("Test", service.lookupPart("Test").getName());
    }


}