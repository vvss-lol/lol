package inventory.service;

import inventory.exceptions.ValidationException;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.repository.Inventory;
import inventory.repository.ProductRepository;
import inventory.validator.PartValidator;
import inventory.validator.ProductValidator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;

@DisplayName("Test service addProduct")
class InventoryServiceTest {
    public static ProductRepository repo;
    public static InventoryService service;
    public static ObservableList<Part> list;

    @BeforeAll
    static void setUp() {
        repo = new ProductRepository("data/products_test.txt", new Inventory());
        service = new InventoryService(null, repo, new ProductValidator(),new PartValidator());
        list = FXCollections.observableArrayList();
        list.add(new OutsourcedPart(1, "Test part", 2.0, 2, 1, 4, "Test company name"));
    }

    @AfterEach
    void tearDown() {
        repo.delete(repo.lookup("Test"));
    }

    @Test
    @Tag("ECP")
    void addProduct_MaxHigherThanMinAndMaxAndMinHigherThanZero_ProductAdded() {
        // ECP
        try{
            service.addProduct("Test", 10, 15, 10, 30, list);
            Assertions.assertNotEquals(repo.lookup("Test"), null);
        }
        catch (Exception e){
            Assertions.fail("No exception should be thrown here");
        }
    }

    @Test
    @Tag("BVA")
    void addProduct_MinMinusMaxEquals1AndMinAndMaxHigherThanZero_ThrowException(){
        //BVA
        Assertions.assertThrows(ValidationException.class, () -> {
            service.addProduct("Test", 10, 1, 2, 1, list);
        }, "ValidationException was expected");
    }


    @Test
    @Tag("ECP")
    void addProduct_MinHigherThanMaxAndMinAndMaxHigherThanZero_ThrowException(){
        //ECP
        Assertions.assertThrows(ValidationException.class, () -> {
            service.addProduct("Test", 15, 12, 15, 10, list);
        }, "ValidationException was expected");
    }

    @Test
    @Tag("ECP")
    void addProduct_MinLowerThanZero_ThrowException(){
        //ECP
        Assertions.assertThrows(ValidationException.class, () -> {
            service.addProduct("Test", 11, 1, -70, 70, list);
        }, "ValidationException was expected");
    }

    @Test
    @Tag("BVA")
    void addProduct_MinEqualZero_ProductAdded(){
        //BVA
        Assertions.assertThrows(ValidationException.class, () -> {
            service.addProduct("Test", 10, 1, 0, 3, list);
        }, "ValidationException was expected");
    }

    @Test
    @Tag("BVA")
    void addProduct_MaxEqualMin_ThrowException(){
        //BVA
        Assertions.assertThrows(ValidationException.class, () -> {
            service.addProduct("Test", 10, 1, 1, 1, list);
        }, "ValidationException was expected");
    }

    @Test
    @Tag("BVA")
    void addProduct_MaxMinusMinEqualOne_ProductAdded(){
        //BVA
        try{
            service.addProduct("Test", 10, 1, 1, 2, list);
            Assertions.assertNotEquals(repo.lookup("Test"), null);
        }
        catch (Exception e){
            Assertions.fail(e.getMessage());
        }
    }

}