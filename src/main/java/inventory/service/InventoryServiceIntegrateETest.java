package inventory.service;

import inventory.exceptions.ValidationException;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.repository.Inventory;
import inventory.repository.PartRepository;
import inventory.validator.PartValidator;
import inventory.validator.ProductValidator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class InventoryServiceIntegrateETest {

    public static PartRepository repo;
    public static InventoryService service;
    public static Inventory inventory;
    public static OutsourcedPart outsourcedPart;

    static final String LOOKUP_PART_NAME = "LookupObject";

    @BeforeAll
    static void setUp() {
        inventory = new Inventory();
        repo = new PartRepository("data/parts_test.txt", inventory);
        service = new InventoryService(repo, null, new ProductValidator(),new PartValidator());
        outsourcedPart = new OutsourcedPart(-1, LOOKUP_PART_NAME, 2.0, 3, 1, 4, "TestCompanyName");
        repo.add(outsourcedPart);
    }

    @AfterAll
    static void tearDown() {
        repo.delete(repo.lookup("Test"));
        repo.delete(repo.lookup(LOOKUP_PART_NAME));
    }

    @Test
    void addOutsourcePart() {
        try {
            service.addOutsourcePart("Test", 1.0, 3, 1, 5, "test_string");
            Part entity = repo.lookup("Test");
            Assertions.assertEquals("Test", entity.getName());
            Assertions.assertEquals(1.0, entity.getPrice());
            Assertions.assertEquals(3, entity.getInStock());
            Assertions.assertEquals(1, entity.getMin());
            Assertions.assertEquals(5, entity.getMax());
        } catch (ValidationException e){
            Assertions.fail("No exception should be thrown here");
        }
    }

    @Test
    void lookupPart() {
        Assertions.assertEquals(service.lookupPart(LOOKUP_PART_NAME), outsourcedPart);
    }
}