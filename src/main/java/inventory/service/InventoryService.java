package inventory.service;

import inventory.exceptions.ValidationException;
import inventory.model.*;
import inventory.repository.Repository;
import inventory.validator.Validator;
import javafx.collections.ObservableList;

public class InventoryService {

    private Repository<Part> partRepo;
    private Repository<Product> productRepo;
    private Validator<Part> partValidator;
    private Validator<Product> productValidator;

    public InventoryService(Repository<Part> partRepo, Repository<Product> productRepo, Validator<Product> productValidator, Validator<Part> partValidator){
        this.partRepo = partRepo;
        this.productRepo = productRepo;
        this.partValidator = partValidator;
        this.productValidator = productValidator;
    }

    public void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue) throws ValidationException {
        InhousePart inhousePart = new InhousePart(partRepo.getAutoId(), name, price, inStock, min, max, partDynamicValue);
        partValidator.validate(inhousePart);
        partRepo.add(inhousePart);
    }

    public void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue) throws ValidationException{
        OutsourcedPart outsourcedPart = new OutsourcedPart(partRepo.getAutoId(), name, price, inStock, min, max, partDynamicValue);
        partValidator.validate(outsourcedPart);
        partRepo.add(outsourcedPart);
    }

    public void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts) throws ValidationException {
        Product product = new Product(productRepo.getAutoId(), name, price, inStock, min, max, addParts);
        this.productValidator.validate(product);
        productRepo.add(product);
    }

    public ObservableList<Part> getAllParts() {
        return partRepo.getAll();
    }

    public ObservableList<Product> getAllProducts() {
        return productRepo.getAll();
    }

    public Part lookupPart(String search) {
        return partRepo.lookup(search);
    }

    public Product lookupProduct(String search) {
        return productRepo.lookup(search);
    }

    public void updateInhousePart(int partIndex, int partId, String name, double price, int inStock, int min, int max, int partDynamicValue) throws ValidationException{
        InhousePart inhousePart = new InhousePart(partId, name, price, inStock, min, max, partDynamicValue);
        partValidator.validate(inhousePart);
        partRepo.update(partIndex, inhousePart);
    }

    public void updateOutsourcedPart(int partIndex, int partId, String name, double price, int inStock, int min, int max, String partDynamicValue)throws ValidationException{
        OutsourcedPart outsourcedPart = new OutsourcedPart(partId, name, price, inStock, min, max, partDynamicValue);
        partValidator.validate(outsourcedPart);
        partRepo.update(partIndex, outsourcedPart);
    }

    public void updateProduct(int productIndex, int productId, String name, double price, int inStock, int min, int max, ObservableList<Part> addParts) throws ValidationException{
        Product product = new Product(productId, name, price, inStock, min, max, addParts);
        productValidator.validate(product);
        productRepo.update(productIndex, product);
    }

    public void deletePart(Part part){
        partRepo.delete(part);
    }

    public void deleteProduct(Product product){
        productRepo.delete(product);
    }

}
