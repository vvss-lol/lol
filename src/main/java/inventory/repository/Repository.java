package inventory.repository;

import javafx.collections.ObservableList;

public interface Repository<T> {

    void read();
    ObservableList<T> getAll();
    void writeAll();
    void add(T t);
    T lookup(String search);
    void update(int index, T el);
    void delete(T el);

    int getAutoId();
}
