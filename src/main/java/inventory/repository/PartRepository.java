package inventory.repository;

import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.util.List;
import java.util.StringTokenizer;

public class PartRepository implements Repository<Part> {

    private final String filename;
    private Inventory inventory;

    public PartRepository(String filename, Inventory inventory){
        this.filename = filename;
        this.inventory = inventory;
        read();
    }

    @Override
    public void read() {
        ClassLoader classLoader = PartRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        ObservableList<Part> listP = FXCollections.observableArrayList();
        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = null;
            while((line=br.readLine())!=null){
                Part part=getPartFromString(line);
                if (part!=null)
                    listP.add(part);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        inventory.setAllParts(listP);
    }

    private Part getPartFromString(String line){
        Part item=null;
        if (line==null|| line.equals("")) return null;
        StringTokenizer st=new StringTokenizer(line, ",");
        String type=st.nextToken();
        if (type.equals("I")) {
            int id= Integer.parseInt(st.nextToken());
            inventory.setAutoPartId(id);
            String name= st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            int idMachine= Integer.parseInt(st.nextToken());
            item = new InhousePart(id, name, price, inStock, minStock, maxStock, idMachine);
        }
        else {
            int id= Integer.parseInt(st.nextToken());
            inventory.setAutoPartId(id);
            String name= st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            String company=st.nextToken();
            item = new OutsourcedPart(id, name, price, inStock, minStock, maxStock, company);
        }
        return item;
    }

    @Override
    public ObservableList<Part> getAll() {
        return inventory.getAllParts();
    }

    @Override
    public void writeAll() {
        ClassLoader classLoader = PartRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        List<Part> parts=getAll();

        try(BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {

            for (Part p:parts) {
                System.out.println(p.toString());
                bw.write(p.toString());
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void add(Part part) {
        inventory.getAllParts().add(part);
        writeAll();
    }

    @Override
    public Part lookup(String search) {
        boolean isFound = false;
        Part pFinal = null;
        for(Part p:inventory.getAllParts()) {
            if(p.getName().contains(search)) {
                if(!isFound){
                    pFinal = p;
                    isFound = true;
                }
            }
        }
        if(!isFound)
            return null;
        return       pFinal;
    }

    @Override
    public void update(int index, Part el) {
        inventory.getAllParts().set(index, el);
        writeAll();
    }

    @Override
    public void delete(Part el) {
        inventory.getAllParts().removeIf(it -> el.getPartId() == it.getPartId());
        writeAll();
    }

    @Override
    public int getAutoId() {
        return inventory.getAutoPartId();
    }
}
