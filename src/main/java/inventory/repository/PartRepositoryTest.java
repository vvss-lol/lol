package inventory.repository;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.ObservableList;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.*;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class PartRepositoryTest {

    public static PartRepository repo;

    @BeforeAll
    static void setUp(){
        repo = new PartRepository("data/parts_test.txt", new Inventory());
    }

    @AfterEach
    void tearDown(){
        List<Part> copy = new ArrayList<>(repo.getAll());
        for(Part el: copy){
            repo.delete(el);
        }
    }

    @Test
    void lookup_2PartsList_ReturnsFoundPart() {
        Part prod = new OutsourcedPart(3, "Test", 2.0, 4, 3, 5, "testCompany");
        Part prod2 = new OutsourcedPart(4, "Tst2", 2.0, 4, 3, 5, "testCompany");
        repo.add(prod);
        repo.add(prod2);
        Assertions.assertEquals(prod, repo.lookup("Test"));
    }

    @Test
    void lookup_2PartsList_NoPartIsFound_ReturnsNull() {
        Part prod = new OutsourcedPart(1, "Test", 2.0, 4, 3, 5, "testCompany");
        Part prod2 = new OutsourcedPart(2, "Tet2", 2.0, 4, 3, 5, "testCompany");
        repo.add(prod);
        repo.add(prod2);
        assertNull(repo.lookup("Test3"));
    }

    @Test
    void lookup_EmptyList_NoPartIsFound_ReturnsNull() {
        assertNull(repo.lookup("Test3"));
    }


    @Test
    void lookup_5PartsList_FoundLastProduct() {
        Part prod = new OutsourcedPart(1, "Test", 2.0, 4, 3, 5, "testCompany");
        Part prod2 = new OutsourcedPart(2, "Tet2", 2.0, 4, 3, 5, "testCompany");
        Part prod3 = new OutsourcedPart(3, "Tst6", 2.0, 4, 3, 5, "testCompany");
        Part prod4 = new OutsourcedPart(4, "Tet4", 2.0, 4, 3, 5, "testCompany");
        Part prod5 = new OutsourcedPart(5, "Tst5", 2.0, 4, 3, 5, "testCompany");
        repo.add(prod2);
        repo.add(prod3);
        repo.add(prod4);
        repo.add(prod5);
        repo.add(prod);
        Assertions.assertEquals(prod, repo.lookup("Test"));
    }
}