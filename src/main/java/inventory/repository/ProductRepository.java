package inventory.repository;

import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.util.StringTokenizer;

public class ProductRepository implements Repository<Product> {

    private final String filename;
    private Inventory inventory;

    public ProductRepository(String filename, Inventory inventory){
        this.filename = filename;
        this.inventory = inventory;
        read();
    }

    @Override
    public void read() {
        ClassLoader classLoader = ProductRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        ObservableList<Product> listP = FXCollections.observableArrayList();
        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = null;
            while((line=br.readLine())!=null){
                Product product=getProductFromString(line);
                if (product!=null)
                    listP.add(product);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        inventory.setProducts(listP);
    }

    private Product getProductFromString(String line){
        Product product=null;
        if (line==null|| line.equals("")) return null;
        StringTokenizer st=new StringTokenizer(line, ",");
        String type=st.nextToken();
        if (type.equals("P")) {
            int id= Integer.parseInt(st.nextToken());
            inventory.setAutoProductId(id);
            String name= st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            String partIDs=st.nextToken();

            StringTokenizer ids= new StringTokenizer(partIDs,":");
            ObservableList<Part> list= FXCollections.observableArrayList();
            while (ids.hasMoreTokens()) {
                String idP = ids.nextToken();
                Part part = lookupPart(idP);
                if (part != null)
                    list.add(part);
            }
            product = new Product(id, name, price, inStock, minStock, maxStock, list);
            product.setAssociatedParts(list);
        }
        return product;
    }

    private Part lookupPart(String search) {
        for(Part p:inventory.getAllParts()) {
            if(p.getName().contains(search))
                return p;
        }
        return null;
    }

    @Override
    public ObservableList<Product> getAll() {
        return inventory.getProducts();
    }

    @Override
    public void writeAll() {
        ClassLoader classLoader = ProductRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        ObservableList<Product> products=inventory.getProducts();

        try(BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {

            for (Product pr:products) {
                String line=pr.toString()+",";
                ObservableList<Part> list= pr.getAssociatedParts();
                int index=0;
                while(index<list.size()-1){
                    line=line+list.get(index).getPartId()+":";
                    index++;
                }
                if (index==list.size()-1)
                    line=line+list.get(index).getPartId();
                bw.write(line);
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void add(Product product) {
        inventory.getProducts().add(product);
        writeAll();
    }

    @Override
    public Product lookup(String search) {
        for(Product p: inventory.getProducts()) {
            if(p.getName().contains(search))
                return p;
        }
        return null;
    }

    @Override
    public void update(int index, Product el) {
        inventory.getProducts().set(index, el);
        writeAll();
    }

    @Override
    public void delete(Product el) {
        inventory.getProducts().remove(el);
        writeAll();
    }

    @Override
    public int getAutoId() {
        return inventory.getAutoProductId();
    }
}
