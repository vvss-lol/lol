package inventory.validator;

import inventory.exceptions.ValidationException;

public interface Validator<T> {
    void validate(T el) throws ValidationException;
}
