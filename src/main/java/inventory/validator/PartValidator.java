package inventory.validator;

import inventory.exceptions.ValidationException;
import inventory.model.Part;

public class PartValidator implements Validator<Part> {

    /**
     * Generate an error message for invalid values in a part
     * Valid part will return an empty string
     * @param el
     * @return
     */
    @Override
    public void validate(Part el) throws ValidationException {
        String errorMessage = "";
        if(el.getName().equals("")) {
            errorMessage += "A name has not been entered. ";
        }
        if(el.getPrice() < 0.01) {
            errorMessage += "The price must be greater than 0. ";
        }
        if(el.getInStock() < 1) {
            errorMessage += "Inventory level must be greater than 0. ";
        }
        if(el.getMin() >= el.getMax()) {
            errorMessage += "The Min value must be less than the Max value. ";
        }
        if(el.getInStock() < el.getMin()) {
            errorMessage += "Inventory level is lower than minimum value. ";
        }
        if(el.getInStock() > el.getMax()) {
            errorMessage += "Inventory level is higher than the maximum value. ";
        }
        if(!errorMessage.equals("")){
            throw new ValidationException(errorMessage);
        }
    }
}
