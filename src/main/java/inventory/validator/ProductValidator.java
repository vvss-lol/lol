package inventory.validator;

import inventory.exceptions.ValidationException;
import inventory.model.Product;

public class ProductValidator implements Validator<Product> {

    /**
     * Generate an error message for invalid values in a product
     * and evaluate whether the sum of the price of associated parts
     * is less than the price of the resulting product.
     * A valid product will return an empty error message string.
     * @param el
     * @return
     */
    @Override
    public void validate(Product el) throws ValidationException {
        double sumOfParts = 0.00;
        String errorMessage = "";
        for (int i = 0; i < el.getAssociatedParts().size(); i++) {
            sumOfParts += el.getAssociatedParts().get(i).getPrice();
        }
        if (el.getName().equals("")) {
            errorMessage += "A name has not been entered. ";
        }
        if (el.getMin() <= 0) {
            errorMessage += "The inventory level must be greater than 0. ";
        }
        if (el.getPrice() < 0.01) {
            errorMessage += "The price must be greater than $0. ";
        }
        if (el.getMin() >= el.getMax()) {
            errorMessage += "The Min value must be less than the Max value. ";
        }
        if(el.getInStock() > el.getMax()) {
            errorMessage += "Inventory level is higher than the maximum value. ";
        }
        if (el.getAssociatedParts().isEmpty()) {
            errorMessage += "Product must contain at least 1 part. ";
        }
        if (sumOfParts > el.getPrice()) {
            errorMessage += "Product price must be greater than cost of parts. ";
        }
        if(!errorMessage.equals("")){
            throw new ValidationException(errorMessage);
        }
    }
}
