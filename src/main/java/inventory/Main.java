package inventory;

import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.*;
import inventory.service.InventoryService;
import inventory.controller.MainScreenController;
import inventory.validator.PartValidator;
import inventory.validator.ProductValidator;
import inventory.validator.Validator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Inventory inventory = new Inventory();
        Repository<Part> partsRepository = new PartRepository("data/parts.txt", inventory);
        Repository<Product> productRepository = new ProductRepository("data/products.txt", inventory);
        Validator<Part> partValidator = new PartValidator();
        Validator<Product> productValidator = new ProductValidator();
        InventoryService service = new InventoryService(partsRepository, productRepository, productValidator, partValidator);
        System.out.println(service.getAllProducts());
        System.out.println(service.getAllParts());
        FXMLLoader loader= new FXMLLoader(getClass().getResource("/fxml/MainScreen.fxml"));

        Parent root=loader.load();
        MainScreenController ctrl=loader.getController();
        ctrl.setService(service);

        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}