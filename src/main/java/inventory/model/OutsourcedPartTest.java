package inventory.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OutsourcedPartTest {

    private OutsourcedPart outsourcedPart;

    @BeforeEach
    void setUp() {
        outsourcedPart = new OutsourcedPart(6,"part1",88,9,9,88,"company");
    }

    @Test
    void getCompanyName() {
        assertEquals("company", outsourcedPart.getCompanyName());
    }

    @Test
    void testToString() {
        assertEquals("O,6,part1,88.0,9,9,88,company", outsourcedPart.toString());
    }
}